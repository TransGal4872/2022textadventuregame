(defun stat-min (arg) (safe-nth 2 arg))
(defun stat-max (arg) (safe-nth 1 arg))

(defclass basestats ()
  ((str 
     :initarg :str
     :accessor stat-str)
   (dex 
     :initarg :dex
     :accessor stat-dex)
   (spd 
     :initarg :spd
     :accessor stat-speed)
   (fort 
     :initarg :fort
     :accessor stat-fort)
   (regen 
     :initarg regen
     :accessor stat-regen)
   (magic 
     :initagr :magic
     :accessor stat-magic)
   (luck 
     :initagr :luck
     :accessor stat-luck)
   (sense 
     :initarg :sense
     :accessor stat-sense)))

(defclass pointstats ()
  ((hp 
     :initarg :hp
     :initform '(1 1)
     :accessor hp)
   (mp 
     :initarg :mp
     :initform '(0 1)
     :accessor mp)
   (sta 
     :initarg :sta
     :initform '(1 1)
     :accessor sta)
   (air 
     :initarg :air
     :initform '(1 1)
     :accessor air)))

(defclass element ()
  ((fire
         :initarg :fire
         :initform 0
         :accessor fire)
   (electricity
                :initarg :electric
                :initform 0
                :accessor electric)
   (air
        :initarg :air
        :initform 0
        :accessor air)
   (ice
        :initarg :ice
        :initform 0
        :accessor ice)
   (water
          :initarg :water
          :initform 0
          :accessor water)
   (earth
          :initarg :earth
          :initform 0
          :accessor earth)
   (metal
          :initarg :metal
          :initform 0
          :accessor metal)
   (life-death
               :initarg :life
               :initarg :death
               :initform 0
               :accessor life
               :accessor death)
   (light-shadow
                 :initarg :life
                 :initarg :shadow
                 :initform 0
                 :accessor :light
                 :accessor :shadow)))
