(defparameter *expect* nil)
(defparameter *player* nil)
(defparameter *playerchars* nil)
(defparameter *parties* nil)
(defparameter *party* nil)
(defparameter *wanderers* nil)
(defparameter *scene* nil)
(defparameter *scenes* nil)
(defparameter *inputline* nil)

(defun function-take)
(defun function-put)
(defun function-throw)
(defun function-go)
(defun function-unknown)

(defgeneric find-noun-token (place name) ())

(defun defrag-list (input)
  (let ((x nil))
    (loop for i in input do
          (if i
            (push i x)
            ()))
    (return-from defrag-list (reverse x))))

(defmethod find-noun-token ((place cons) name)
  (let ((x nil))
    (loop for i in place do
          (let ((y (find-noun-token (eval i) name)))
            (push (cons i (defrag-list y) x)))
          (return-from find-noun-token x))))

(defun travel ()
  (let ((x (cdr (assoc (cdr *input-line*) (exits *scene*)))))
    (if x
      (progn
        (setq *scene* x)
        (let ((y (cdr (assoc "on-enter" *scene*))))
          (case (type-of y)
            (('CONS) (loop for f in y (funcall f)))
            (('FUNCTION) (funcall y))
            (otherwise ()))))
      (write "can't go in that direction"))))

(defun pick ()
  (progn
    (assert (equal (car *input-line*) "pick"))
    (pop *input-line*)
    (case (second *input-line*)
      (("up") (progn
                (setf (car *input-line*) "pick up")
                (take)))
      (("lock") (progn
                  (setf (car *input-line*) "pick lock")
                  (pick-lock)))
      (("nose") (write "ewww")))))

(defun verbparse ()
  (case (car (*input-line*))
    (('go 'travel) (game:travel))
    (('walk 'run) (walk))
    (('fly) (fly))
    (('swim) (swim))
    (('take) (take))
    (('put) (put))
    (('look) (look))
    (('use) (use))
    (('eat) (eat))
    (('drink 'quaff) (drink))
    (('switch) (switch))
    (('arm 'equip) (equip))
    (('disarm 'unequip) (unequip))
    (('throw) (game/throw))
    (('pick) (pick))
    (('search) (game/search))
    (otherwise (find-verb-token))))

(defun commandprompt ()
  (loop
    (setq *input-line* strtok)
    (if *input-line*
      (let ((expected (cdr (assoc (car *input-line*) *expecting*))))
        (if expected
          (funcall expected)
          ()))
      (verb-parse))))
