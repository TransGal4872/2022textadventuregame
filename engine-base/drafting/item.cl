(defclass baseitem (baseclass)
  ((nouns :type noun-object
          :initarg :noun
          :accessor noun)
   (inherits
     :initarg :inherits
     :initform nil
     :accessor inherits)))

(defclass item ()
  ((inherits
     :initarg :inherits
     :accessor inherits)
   (quantity :type integer
             :initarg :quantity
             :initform 1
             :accessor quantity)))

(defmethod save ((x item))
  `(make-instance 'item
                  :inherits ,(save (inherits x))
                  :quantity ,(save (quantity x))))

(defclass legendaryitem (baseitem)
  ((name
     :initarg :name
     :initform nil
     :accessor name)
   (pronouns
     :initarg :pronouns
     :initform (it/its)
     :accessor pronouns)))

