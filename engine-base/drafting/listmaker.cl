(defmacro listmaker1 (input)
  (return-from listmaker1
               (cons 'list
                     (cons ''list
                           (loop
                             for i in input
                             collect (if (consp i)
                                       (listmaker1 i)
                                       i))))))

(defun listmaker (&rest input) (listmaker1 input))
