;(declaim (optimize (debug 3)))

(defun lexup (pattern input)
  (if input
    (typecase (car pattern)
      (null
        (return-from lexup nil))
      (boolean
        (if (equal (car input) (car pattern))
          (return-from lexup (cons (car input) (lexup (cdr pattern) (cdr input))))
          (return-from lexup nil)))
      (cons
        (let ((left (lexup (car pattern) input)) (right (lexup (cdr pattern) input)))
          (case (+ (if left 1 0) (if right 2 0))
            (0 (return-from lexup nil))
            (1 (return-from lexup left))
            (2 (return-from lexup right))
            (3 (return-from lexup (list left right))))))
      (string
        (if (equal (car pattern) (car input))
          (return-from lexup (cons (car input) (lexup (cdr pattern) (cdr input))))
          (return-from lexup nil)))
      (symbol
        (if (equal (car pattern) (car input))
          (return-from lexup (cons (car input) (lexup (cdr pattern) (cdr input))))
          (return-from lexup nil)))
      (fixnum
        (if (equal (car pattern) (car input))
          (return-from lexup (cons (car input) (lexup (cdr pattern) (cdr input))))
          (return-from lexup nil)))
      (standard-char
        (if (equal (car pattern) (car input))
          (return-from lexup (cons (car input) (lexup (cdr pattern) (cdr input))))
          (return-from lexup nil)))
      (otherwise (return-from lexup nil)))
    (typecase (car pattern)
      (nil (return-from lexup nil))
      (boolean (return-from lexup (list (cdr pattern))))
      (cons (return-from lexup (lexup (car pattern) nil)))
      (otherwise (return-from lexup nil)))))


(defun add-pattern-leaf (bin ptr)
  (if (and (equal (type-of (car bin)) 'boolean) (not (null ptr)))
    (unless (position ptr (cdr bin))
      (progn
        (if (typep ptr 'list)
          (rplacd (last bin) ptr)
          (rplacd (last bin) (list ptr)))
        (return-from add-pattern-leaf (last bin 0))))))

(defun extend-pattern-car (pattern input)
  (unless (null input)
    (if (equal (cadr pattern) (car input))
      (return-from extend-pattern-car (list (car input) (extend-pattern-cdr (cdr pattern) (cdr input))))
      (typecase (caar pattern)
        (null
          (progn
            (setf (caar pattern) (list `(nil) input))
            (return-from extend-pattern-car input)))
        (boolean
          (if (equal (caar pattern) (car input))
            (return-from extend-pattern-car (list (car input) (add-pattern-leaf (car pattern) (cdr input))))))
        (cons
          (return-from extend-pattern-car (list (car input) (extend-pattern-car (car pattern) input))))
        (string
          (if (equal (caar pattern) (car input))
            (return-from extend-pattern-car (list (car input) (extend-pattern-cdr (cdar pattern) (cdr input))))
            (progn
              (push input (car pattern))
              (return-from extend-pattern-car input))))
        (fixnum
          (if (equal (caar pattern) (car input))
            (return-from extend-pattern-car (list (car input) (extend-pattern-cdr (cdar pattern) (cdr input))))
            (progn
              (push input (car pattern))
              (return-from extend-pattern-car input))))
        (symbol
          (if (equal (caar pattern) (car input))
            (return-from extend-pattern-car (list (car input) (extend-pattern-cdr (cdar pattern) (cdr input))))
            (progn
              (push input (car pattern))
              (return-from extend-pattern-car input))))
        (standard-char
          (if (equal (caar pattern) (car input))
            (return-from extend-pattern-car (list (car input) (extend-pattern-cdr (cdar pattern) (cdr input))))
            (progn
              (push input (car pattern))
              (return-from extend-pattern-car input))))
        (otherwise (return-from extend-pattern-car #()))))
    (return-from extend-pattern-car nil)))

(defun extend-pattern-cdr (pattern input)
  (unless (null input)
    (typecase (cadr pattern)
      (null
        (progn
          (setf (cdr pattern) (list `(nil) input))
          (return-from extend-pattern-cdr input)))
      (boolean
        (if (equal (cadr pattern) (car input))
          (progn
            (add-pattern-leaf (cdr pattern) (cdr input))
            (return-from extend-pattern-cdr t))))
      (cons
        (return-from extend-pattern-cdr (list (car input) (extend-pattern-car (cdr pattern) input))))
      (string
        (if (equal (cadr pattern) (car input))
          (return-from extend-pattern-cdr (list (car input) (extend-pattern-cdr (cddr pattern) (cdr input))))
          (progn
            (push input pattern)
            (return-from extend-pattern-cdr input))))
      (fixnum
        (if (equal (cadr pattern) (car input))
          (return-from extend-pattern-cdr (list (car input) (extend-pattern-cdr (cddr pattern) (cdr input))))
          (progn
            (push input (cdr pattern))
            (return-from extend-pattern-cdr input))))
      (symbol
        (if (equal (cadr pattern) (car input))
          (return-from extend-pattern-cdr (list (car input) (extend-pattern-cdr (cddr pattern) (cdr input))))
          (progn
            (push input (cdr pattern))
            (return-from extend-pattern-cdr input))))
      (standard-char
        (if (equal (cadr pattern) (car input))
          (return-from extend-pattern-cdr (list (car input) (extend-pattern-cdr (cddr pattern) (cdr input))))
          (progn
            (push input (cdr pattern))
            (return-from extend-pattern-cdr input))))
      (otherwise (return-from extend-pattern-cdr #())))
    (return-from extend-pattern-cdr nil)))

(defun extend-pattern (pattern input)
  (unless (null input)
    (typecase (car pattern)
      (null
        (progn
          (setf pattern (list `(nil) input))
          (return-from extend-pattern input)))
      (boolean
        (if (equal (car pattern) (car input))
          (progn
            (add-pattern-leaf (cdr pattern) (cdr input))
            (return-from extend-pattern input))))
      (cons
        (return-from extend-pattern (extend-pattern-car pattern input)))
      (string
        (if (equal (car pattern) (car input))
          (return-from extend-pattern (list (car input) (extend-pattern-cdr pattern (cdr input))))
          (progn
            (rplacd pattern (cons (car pattern) (cdr pattern)))
            (rplaca pattern input)
            (return-from extend-pattern input))))
      (fixnum
        (if (equal (car pattern) (car input))
          (return-from extend-pattern (list (car input) (extend-pattern-cdr pattern (cdr input))))
          (progn
            (rplacd pattern (cons (car pattern) (cdr pattern)))
            (rplaca pattern input)
            (return-from extend-pattern input))))
      (symbol
        (if (equal (car pattern) (car input))
          (return-from extend-pattern (list (car input) (extend-pattern-cdr pattern (cdr input))))
          (progn
            (rplacd pattern (cons (car pattern) (cdr pattern)))
            (rplaca pattern input)
            (return-from extend-pattern input))))
      (standard-char
        (if (equal (car pattern) (car input))
          (return-from extend-pattern (list (car input) (extend-pattern-cdr pattern (cdr input))))
          (progn
            (rplacd pattern (cons (car pattern) (cdr pattern)))
            (rplaca pattern input)
            (return-from extend-pattern input))))
      (otherwise return-from extend-pattern #()))
    (return-from extend-pattern nil)))

(defun multiextend-pattern (pattern &rest strs)
  (loop
    for str in strs
    collect (extend-pattern pattern str)))

(defun defpattern (&rest strs)
  (let ((pattern (car strs)))
    (loop
      for str in (cdr strs)
      collect (extend-pattern pattern str))))

;(step
(progn
  (defvar x `(1 2 3 4 5 6 7 8 9 10))
  (multiextend-pattern x `(1 1 2 3 5 7) `(1 2 4 8 16) `(1 3 5 7 9) `(2 4 6 8 10) `(3 6 9 12 15 18) (list 1 2 3 t 'foo) (list 1 2 3 t 'bar))
  (print x)
  (print (leaves x))
  (print (multileaves x))
  (print (lexup x `(1 2 3))))
;)
