(defun strtok (&optional (stream *standard-input*))
  (let
    ((input (coerce (read-line stream) 'list))
     (output (list "")))
    (progn
      (loop while (first input)
            do (if (>=
                     #x20
                     (char-code (first input)))
                 (progn
                   (if (string= (car (last output)) "")
                     ()
                     (rplacd (last output) (list "")))
                   (pop input))
                 (rplaca
                   (last output)
                   (concatenate
                     'string
                     (car (last output))
                     (list (pop input))))))
      (return-from strtok output))))
