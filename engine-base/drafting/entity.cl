(defclass entity (ibaseclass movetypes basestats pointstats noun-object pronoun-object)
  ((name
     :initarg :name
     :accessor name)
   (inventory :type list
              :initarg :inventory
              :initform nil
              :accessor inventory)
   (equipment :type list
              :initarg :equipment
              :initform nil
              :accessor equipment)))


(defclass entity-template (baseclass)
  ((noun
         :initarg :noun
         :accessor noun)
   (stats
          :initarg :stats)
   (points
           :initarg :points)
   (movement
             :initarg :movetypes)
   (element
   (inherits
     :initarg :inherits
     :accessor inherits
     :initform nil)
   (equip-slots :type list
                :initarg :equip-slots
                :accessor equip-slots)))
