(defun tokenize (s)
    (let ((digits
            '(#\0 #\1 #\2 #\3 #\4 #\5 #\6 #\7 #\8 #\9))
          (hexdigits
            '(#\0 #\1 #\2 #\3 #\4 #\5 #\6 #\7 #\8 #\9 #\a #\A #\b #\B #\c #\C #\d #\D #\e #\E #\f #\F))
          (octdigits
            '(#\0 #\1 #\2 #\3 #\4 #\5 #\6 #\7)))
      (if (equal (length s) 1)
        (if (position (char s 0) digits)
          (return-from tokenize (read-from-string s))
          (return-from tokenize s))
        (case (char s 0)
          ((#\# #\0)
           (case (char s 1)
             ((#\x #\X) (loop
                          for i across (subseq s 2)
                          do (progn
                               (setf (char s 0) #\#)
                               (unless (position i hexdigits)
                                 (return-from tokenize s))
                               (return-from tokenize (read-from-string s)))))
             ((#\o #\O) (loop
                          for i across (subseq s 2)
                          do (progn
                               (setf (char s 0) #\#)
                               (unless (position i octdigits)
                                 (return-from tokenize s))
                               (return-from tokenize (read-from-string s)))))
             ((#\b #\B) (loop
                          for i across (subseq s 2)
                          do (progn
                               (setf (char s 0) #\#)
                               (unless (position i '(#\0 #\1))
                                 (return-from tokenize s))
                               (return-from tokenize (read-from-string s)))))
             (otherwise (loop
                          for i across s
                          do (progn
                               (unless (position i digits)
                                 (return-from tokenize s))
                               (return-from tokenize (read-from-string s)))))))
          (#\- (loop
                 for i across (subseq s 1)
                 do (progn
                      (unless (position i digits)
                        (return-from tokenize s))
                      (return-from tokenize (read-from-string s)))))
          (otherwise (loop
                       for i across s
                       do (progn
                            (unless (position i digits)
                              (return-from tokenize s))
                            (return-from tokenize (read-from-string s)))))))))
