;used
(defmacro ifnot (test if-false &optional if-true)
  `(if (not ,test)
     ,if-false
     ,if-true))

;used
(defmacro if* (if-true &optional if-false)
  (let ((tmp (eval if-true))) `(if ,tmp ,tmp ,if-false)))

;used
(defmacro return-when (if-true) (let ((tmp (eval if-true))) `(when ,tmp (return ,tmp))))

(defun strict-falsep (thing) (typecase
                        (null (return-from strict-falsep t))
                        (cons (return-from strict-falsep (and (strict-falsep (car thing)) (strict-falsep (cdr thing)))))
                        (t (return-from falsep nil))))

(defun loose-truep (thing) (not (strict-falsep thing)))

(defun falsep (thing) (typecase
                        (null (return-from falsep t))
                        (cons (return-from falsep (and (falsep (car thing)) (falsep (cdr thing)))))
                        ('(simple-vector 0) (return-from falsep t))
                        ('(simple-bit-vector 0) (return-from falsep t))
                        ('(simple-array * (0)) (return-from falsep t))
                        (t (return-from falsep nil))))

(defun truep (thing) (not (falsep thing)))

(defun cfalsep (thing) (typecase
                        (null (return-from cfalsep t))
                        (cons (return-from cfalsep (and (cfalsep (car thing)) (cfalsep (cdr thing)))))
                        ('(simple-vector 0) (return-from cfalsep t))
                        ('(simple-vector *) (return-from cfalsep (cfalsep (coerce thing 'list))))
                        ('(simple-bit-vector 0) (return-from cfalsep t))
                        ('(simple-bit-vector *) (return-from cfalsep (cfalsep (coerce thing 'list))))
                        ('(simple-array * (0)) (return-from cfalsep t))
                        ('(simple-array * *) (return-from cfalsep (coerce thing 'list)))
                        (bit (eq thing 0))
                        (t (return-from cfalsep nil))))

(defun ctruep (thing) (not (cfalsep thing)))

;used???
(defun leaves (tree)
  (if tree
    (if (consp (car tree))
      (return-from leaves (cons (leaves (car tree)) (leaves (cdr tree))))
      (if (cdr tree)
        (return-from leaves (leaves (cdr tree)))
        (return-from leaves tree)))
    (return-from leaves nil)))

;used???
(defun multileaves (tree)
  (if tree
    (ifnot (typep (car tree) 'boolean)
          (if (consp (car tree))
            (return-from multileaves (cons (multileaves (car tree)) (multileaves (cdr tree))))
            (if (cdr tree)
              (return-from multileaves (multileaves (cdr tree)))
              (return-from multileaves tree)))
          (return-from multileaves (cdr tree)))
    (return-from multileaves nil)))
(defun value (thing) (return-from value thing))

(defun safecar (thing)
  (if (listp thing)
    (car thing)
    (return-from safecar thing)))

;used
(defun firstatom (thing)
  (if (listp thing)
    (firstatom thing)
    (return-from firstatom thing)))

(defun safecdr (thing)
  (if (listp thing)
    (cdr thing)
    (return-from safecdr nil)))

(defun cdr-or-self (thing)
  (if (listp thing)
    (cdr thing)
    (return-from cdr-or-self thing)))

(defun safenth (n thing)
  (if (listp thing)
    (nth n thing)
    (if (= n 0)
      (return-from safenth thing)
      (return-from safenth nil))))

(defun safenthcdr (n thing)
  (if (listp thing)
    (nthcdr n thing)
    (if (= n 0)
      (return-from safenthcdr thing)
      (return-from safenthcdr nil))))

(defun safelast (thing &optional (n 1))
  (if (listp thing)
    (last thing n)
    (if (= n 0)
      (return-from safelast thing)
      (return-from safelast (cons thing nil)))))

(defun safebutlast (thing n &optional (n 1))
  (if (listp thing)
    (butlast thing n)
    (return-from safebutlast thing)))

;used
(defun lastcar (thing)
  (return-from lastcar (car (last thing))))

(defun safelastcar (thing)
  (if (listp thing)
    (lastcar thing)
    (return-from safelastcar thing)))

(defun pop-back (thing) (if (consp thing)
                          (if (last thing 0)
                            (let ((tmp (last thing 0)))
                              (progn
                                (rplacd (last thing 2) (lastcar thing))
                                (return-from pop-back tmp)))
                            (let ((tmp (car (last thing))))
                              (progn
                                (rplacd (last thing 2) nil)
                                (return-from pop-back nil))))))

(defun push-back* (newthing thing) (typecase thing
                                     (cons
                                       (if (last thing 0)
                                         (rplacd
                                           (last thing)
                                           (cons (last thing 0) newthing))
                                         (rplacd
                                           (last thing)
                                           (cons newthing nil))))
                                     (string
                                       (setf thing (coerce `(,@(coerce thing 'list) newthing) 'string)))
                                     (simple-vector
                                       (setf thing (coerce `(,@(coerce thing 'list) newthing) 'simple-vector)))
                                     (vector
                                       (vector-push-extend newthing thing))
                                     (t
                                       (setf thing (cons thing newthing)))))

;used
(defun push-back (newthing thing) (if (consp thing)
                                    (if (last thing 0)
                                      (rplacd
                                        (last thing)
                                        (cons (last thing 0) newthing))
                                      (rplacd
                                        (last thing)
                                        (cons newthing nil)))
                                    (setf thing (list thing newthing))))

(defun push-cdr (newthing thing) (if (consp thing)
                                   (rplacd (last thing) (cons (last thing 0) newthing))
                                   (setf thing (cons thing newthing))))

(defun pop-cdr (thing) (if (consp thing)
                         (let ((tmp (last thing 0)))
                           (progn
                             (rplacd (last thing 2) (lastcar thing))
                             (return-from pop-cdr tmp)))
                         (let ((tmp thing))
                           (progn
                             (setf thing nil)
                             (return-from pop-cdr tmp)))))

(defun bool (x) (not (null x)))

(defun nor (x y) (and (not x) (not y)))

(defun nand (x y) (or (not x) (not y)))

(defun eqv (x y) (or (and x y) (nor x y)))

(defun xor (x y) (not (iff x y)))

(defun implies (x y) (if x (bool y) (bool x)))

;used???
(defun get-random-element (arg)
  (if (listp arg)
    (return-from get-random-element (nth (random (length arg)) arg))
    (return-from get-random-element arg)))
