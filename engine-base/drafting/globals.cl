;;;; notes
;;; $;;;; H1
;;; $;;; Description
;;;
;;; $;;; H2
;;; $;; Description
;;;
;;; ;; Block Comment (/*)
;;; ; Description
;;;
;;; ; Comment (//)
;;;
;;; *name* global parameter
;;; (*name*) global accessor macro
;;; <name> class
;;; +name+ constant or prototype
;;; (slot-of object) accessor

(defparameter *players* nil)
(defparameter *playerid* 0)
(defmacro *player* () `(svref *players* *playerid*))

(defparameter *partyids* nil)
(defmacro party () `(loop
                          for i in *partyids*
                          collect (svref *players* i)))
(defmacro *party* () `(values-list *party*))

(defparameter *scenes* nil)
(defparameter *sceneid* 0)
(defmacro *scene* () `(svref *scenes* *sceneid*))

(defparameter *player-scene* nil)

(defparameter *inputline* nil)
