(defclass noun-object ()
  ((singular :type list
             :initarg :singular
             :accessor single-noun)
   (plural :type list
                :initarg :plural
                :accessor plural-noun)
   (collective :type list
                    :initarg :collective
                    :accessor collective-noun)))

(defclass pronoun-object ()
  ((subject :type string
            :initarg :subject
            :accessor subject-pronoun)
   (object :type string
           :initarg :object
           :accessor object-pronoun)
   (determiner :type string
               :initarg :determiner
               :accessor determiner-pronoun)
   (posessive :type string
              :initarg :posessive
              :accessor posessive-pronoun)
   (reflexive :type string
              :initarg :reflexive
              :accessor reflexive-pronoun)))

(defmacro it-its () (make-instance 'pronoun-object
                                   :subject "it"
                                   :object "it"
                                   :determiner "its"
                                   :posessive "its"
                                   :reflexive "itself"))

(defmacro he-him () (make-instance 'pronoun-object
                                   :subject "he"
                                   :object "him"
                                   :determiner "his"
                                   :posessive "his"
                                   :reflexive "himself"))

(defmacro she-her () (make-instance 'pronoun-object
                                    :subject "she"
                                    :object "her"
                                    :determiner "her"
                                    :posessive "hers"
                                    :reflexive "herself"))

(defmacro they-them () (make-instance 'pronoun-object
                                      :subject "they"
                                      :object "them"
                                      :determiner "their"
                                      :posessive "theirs"
                                      :reflexive "themself"))

(defmacro plural () (make-instance 'pronoun-object
                                   :subject "they"
                                   :object "them"
                                   :determiner "their"
                                   :posessive "theirs"
                                   :reflexive "themselves"))
