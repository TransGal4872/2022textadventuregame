(defgeneric save (x))

(defmethod save (x) (return-from save x))

(defmethod save ((x cons))
  `(cons 'list ,(loop
                  for i in x
                  collect (save i))))
