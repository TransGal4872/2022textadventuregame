(defclass <container> (<ibaseclass>)
  ((name :type list
         :initarg :name
         :accessor name)
   (items :type list
          :initarg :items
          :initform nil
          :accessor items)))

(defclass <scenery> (<ibaseclass>)
  ((name :type list
         :initarg :name
         :accessor name)))

(defclass <exitverb> ()
  ((name :type string
         :initarg :name
         :accessor name)
   (exittype :type <movetypes>
             :initarg :exittype
             :accessor exittype)))

(defclass <exit> ()
  ((name :type string
         :initarg :name
         :accessor name)
   (verbs :type list
          :initarg :verbs
          :accessor verbs)
   (room :type integer
         :initarg :room
         :accessor room)))

(defclass <templatescene> (<ibaseclass>)
  ((scenery :type list
             :initarg :scenery
             :initform nil
             :accessor scenery)))

(defclass <scene> (<templatescene>)
  ((exits :type list
          :initarg :exits
          :accessor exits)
   (containers :type list
               :initarg :containers
               :initform nil
               :accessor containers)
   (entities :type list
             :initarg :entities
             :initform nil
             :accessor entities)))

(defclass <floorscene> (<scene>)
   ((items :type list
          :initarg :items
          :initform nil
          :accessor items)))
