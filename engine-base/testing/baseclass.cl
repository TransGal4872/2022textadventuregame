(declaim (optimize safety))

(defclass <baseclass> ()
  ((descript :type list
     :initarg :descript
             :initform nil
     :accessor descript)
   (actions :type list
            :initarg :actions
            :initform nil
            :accessor actions)
   (events :type list
           :initarg :events
           :initform nil
           :accessor events)
   (weight :type '(or fixnum boolean)
     :initarg :weight
     :initform t
     :accessor weight)))

(defmethod baseclass/* ((object <baseclass>))
  (make-instance '<baseclass>
                 :descript (slot-value object 'descript)
                 :actions (slot-value object 'actions)
                 :events (slot-value object 'events)
                 :weight (slot-value object 'weight)))

(defmethod shared-initialize :before ((object <baseclass>) slot-names &rest initargs &key ((:baseclass/ object) '<baseclass>) &allow-other-keys)
  (shared-initialize '(:descript :actions :events :weight)
                     :descript (slot-value object 'descript)
                     :actions (slot-value object 'actions)
                     :events (slot-value object 'events)
                     :weight (slot-value object 'weight)))

(defmethod short-descript (&optional (object <baseclass>))
  (progn
    (unless object (setq object (*scene*)))
    (car (descript object))))

(defmethod (setf short-descript) ((newval string) &optional (object <baseclass>))
  (progn
    (unless object (setq object (*scene*)))
    (setf (car (descript object)) newval)))

(defmethod long-descript (&optional (object <baseclass>))
  (progn
    (unless object (setq object (*scene*)))
    (lastcar (descript object))))

(defmethod (setf long-descript) ((newval string) &optional (object <baseclass>))
  (progn
    (unless object (setq object (*scene*)))
    (if (second (descript object))
      (setf (second (descript object)) newval)
      (push-back newval (descript object)))))

(defclass <ibaseclass> (<baseclass>)
  ((inherits :type list
            :initarg :inherits
            :initform nil
            :accessor inherits)))

(defmethod ibaseclass/* ((object <ibaseclass>))
  (make-instance '<ibaseclass>
                 :descript (slot-value object 'descript)
                 :actions (slot-value object 'actions)
                 :events (slot-value object 'events)
                 :weight (slot-value object 'weight)
                 :inherits (slot-value object 'inherits)))

(defmethod shared-initialize :before ((object <ibaseclass>) slot-names &rest initargs &key ((:ibaseclass/ object) '<ibaseclass>) &allow-other-keys)
  (shared-initialize '(:descript :actions :events :weight :inherits)
                     :descript (slot-value object 'descript)
                     :actions (slot-value object 'actions)
                     :events (slot-value object 'events)
                     :weight (slot-value object 'weight)
                     :inherits (slot-value object 'inherits)))

(defmethod weight ((object <ibaseclass>))
  (if*
    (slot-value object 'weight)
    (when (inherits object)
      (loop
      for i in (inherits object)
      do (return-when (weight (eval i))))))

(defmethod actions ((object <ibaseclass>))
  (let ((x (slot-value object 'actions)))
    (progn
    (loop
      for i in (inherits object)
      do (loop
           for j in (actions (eval i))
           do (unless (assoc (car j) (slot-value object 'actions)) (push-back x j))))
    (return-from actions x))))

(defmethod events ((object <ibaseclass>))
  (let ((x (slot-value object 'events)))
    (progn
    (loop
      for i in (inherits object)
      do (loop
           for j in (events (eval i))
           do (unless (assoc (car j) (slot-value object 'events)) (push-back x j))))
    (return-from events x))))

(defmethod short-descript (&optional (object <ibaseclass>))
  (progn
    (unless object (setq object (*scene*)))
    (if (descript object)
      (car (descript object))
      (when (inherits object)
        (car (loop
               for i in (inherits object)
               do (return-when (descript (eval i)))))))))

(defmethod long-descript (&optional (object <ibaseclass>))
  (progn
    (unless object (setq object (*scene*)))
    (if (descript object)
      (car (descript object))
      (when (inherits object)
        (car (loop
               for i in (inherits object)
               do (return-when (descript (eval i)))))))))
