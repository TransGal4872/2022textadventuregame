(defclass <movetypes> ()
  ((walk
     :initarg :walk
     :accessor walk)
   (swim
     :initarg :swim
     :accessor swim)
   (fly
     :initarg :fly
     :accessor fly)
   (phase-move
     :initarg :phase-move
     :accessor phase-move)
   (teleport
     :initarg :teleport
     :accessor teleport)))

(defmethod shared-initialize :before ((object <movetypes>) slot-names &rest initargs &key ((:movetypes/* object) '<movetypes>) &allow-other-keys)
  (shared-initialize '(:walk :swim :fly :phase-move :teleport)
                     :walk (walk object)
                     :swim (swim object)
                     :fly (fly object)
                     :phase-move (phase-move object)
                     :teleport (teleport object)))

(defmethod movetypes/* ((object <movetypes>)) object)

(defmethod (setf movetypes/*) ((newvalue <movetypes>) (object <movetypes>))
  (progn
  (setf (walk object) (walk newvalue))
  (setf (swim object) (swim newvalue))
  (setf (fly object) (fly newvalue))
  (setf (phase-move object) (phase-move newvalue))
  (setf (teleport object) (teleport newvalue))))
